<?php
require_once './PHP/Connection/connection.php';
if ( isset( $_GET['editInfo'] ) ) {
    try {
        $sl = $_GET['editInfo'];
        $sql = "SELECT bookmark_name, bookmark_url from bookmark_list WHERE slno = $sl";
        $stm = $conn->query( $sql );
        $result = $stm->fetchAll( PDO::FETCH_ASSOC );
        $bookmark_name = $result[0]['bookmark_name'];
        $bookmark_url = $result[0]['bookmark_url'];
    } catch ( PDOException $e ) {
        echo "Error: ".$e->getMessage();
    }

}
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Edit Bookmark</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="./CSS/style.css" />
    <link rel="stylesheet" href="./CSS/bootstrap.min.css" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
      integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
  </head>

  <body>
    <div class="container roboto">
      <form
        class="f1bg m-auto mt-5 p-5"
        action="./PHP/Update/UpdateQuery.php"
        method="post"
      >
        <h5 class="mx-3">Update Your bookmark</h5>
        <div class="row">
          <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="m-3">
              <input
                class="form-control"
                type="text"
                name="updateBookmarkname"
                id="updateBookmarkname"
                value="<?php echo $bookmark_name ?>"
              />
              <input
                class="form-control" style="display:none;"
                type="text"
                name="sl"
                value="<?php echo $sl ?>"
              />
            </div>
          </div>
          <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="m-3">
              <input
                class="form-control"
                type="text"
                name="updateBookmarkurl"
                id="updateBookmarkurl"
                value="<?php echo $bookmark_url ?>"
              />
            </div>
          </div>
          <div class="col-sm-12 col-md-1 col-lg-1">
            <div class="m-3">
              <button
                class="btn btn-outline-secondary border-1 rounded-1"
                name="updateButton"
                type="submit"
              >Update</button>
            </div>
          </div>
        </div>
      </form>