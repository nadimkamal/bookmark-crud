<?php
require_once "./PHP/Connection/connection.php";
require_once "./PHP/Show/ShowbookMark.php";
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Bookmark</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="./CSS/style.css" />
    <link rel="stylesheet" href="./CSS/bootstrap.min.css" />
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
      integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
      crossorigin="anonymous"
      referrerpolicy="no-referrer"
    />
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
  </head>

  <body>
    <div class="container roboto">
      <form
        class="f1bg m-auto mt-5 p-5"
        action="./PHP/ADD/AddQuery.php"
        method="post"
      >
        <h5 class="mx-3">Add Your bookmark</h5>
        <div class="row">
          <div class="col-sm-12 col-md-3 col-lg-3">
            <div class="m-3">
              <input
                class="form-control"
                type="text"
                name="Bookmarkname"
                id="Bookmarkname"
                placeholder="Name"
              />
            </div>
          </div>
          <div class="col-sm-12 col-md-8 col-lg-8">
            <div class="m-3">
              <input
                class="form-control"
                type="text"
                name="Bookmarkurl"
                id="Bookmarkurl"
                placeholder="URL"
              />
            </div>
          </div>
          <div class="col-sm-12 col-md-1 col-lg-1">
            <div class="m-3">
              <button
                class="btn btn-outline-secondary border-1 rounded-1"
                name="addButton"
                type="submit"
              >Add</button>
            </div>
          </div>
        </div>
      </form>
<br><br><br><br>
      <?php
showBookMarks( $conn );
?>
    </div>
  </body>
</html>
