<?php
require_once '../Connection/connection.php';

if ( isset( $_GET['deleteInfo'] ) ) {
    try {
        $sl = $_GET['deleteInfo'];
        $deleteQuery = "DELETE FROM bookmark_list WHERE slno=$sl";
        $conn->exec( $deleteQuery );
        header( 'location:../../index.php' );
    } catch ( PDOException $e ) {
        echo "Error: ".$e->getMessage();
    }
}

?>