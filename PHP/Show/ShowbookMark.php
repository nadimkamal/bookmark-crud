<?php

function showBookMarks( $conn ) {
    try {
        $showQuery = "SELECT * from bookmark_list";
        $stm = $conn->query( $showQuery );
        $result = $stm->fetchAll( PDO::FETCH_ASSOC );

        echo "<table class='table table-striped my-5'>
        <thead>
          <tr>
            <th>Slno.</th>
            <th>Book Mark Name</th>
            <th>Book Mark URL</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>";

        $i = 1;
        foreach ( $result as $row ) {
            $id = $row['slno'];
            $name = $row['bookmark_name'];
            $url = $row['bookmark_url'];

            echo "<tr>
            <td class='col-1'>$i</td>
            <td class='col-3'>$name</td>
            <td class='col'>$url</td>
            <td class='col-3'>
            <a class='btn btn-outline-secondary py-0 px-2' href='editBookMark.php?editInfo=$id'><i class='fa-solid fa-pen-to-square'></i></a>
            <a class='btn btn-outline-secondary py-0 px-2 mx-1'><i class='fa-solid fa-eye'></i></a>
            <a class='btn btn-outline-danger py-0 px-2' href='./PHP/Delete/deleteQuery.php?deleteInfo=$id'><i class='fa-solid fa-trash-can'></i></a>
            </td>

          </tr>";
            $i++;
        }
        echo "</tbody>
      </table>";
    } catch ( PDOException $e ) {
        echo "".$e->getMessage();
    }
}
?>