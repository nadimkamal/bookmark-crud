<?php
require_once '../Connection/connection.php';
if ( isset( $_POST['updateButton'] ) ) {
    try {
        $sl = $_POST['sl'];
        $bookmark_name = $_POST['updateBookmarkname'];
        $bookmark_url = $_POST['updateBookmarkurl'];

        if ( empty( $bookmark_name ) || empty( $bookmark_url ) ) {
            echo "<script>alert('Name and URL Field are Required Before Update')</script>";
        } else {
            $updateQuery = "UPDATE bookmark_list SET bookmark_name = '$bookmark_name',bookmark_url = '$bookmark_url' WHERE slno = $sl";
            $conn->exec( $updateQuery );
            header( "location:../../index.php" );
        }
    } catch ( PDOException $e ) {
        echo "Error: ".$e->getMessage();
    }
}
?>