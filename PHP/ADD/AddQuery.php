<?php
require_once '../Connection/connection.php';

if ( isset( $_POST['addButton'] ) ) {

    try {
        $bookmark_name = $_POST['Bookmarkname'];
        $bookmark_url = $_POST['Bookmarkurl'];

        if ( empty( $bookmark_name ) || empty( $bookmark_url ) ) {
            echo "<script>alert('Name and URL Field are Required Before ADD');</script>";
        } else {
            $insertQuery = "INSERT INTO bookmark_list(bookmark_name,bookmark_url) VALUES('$bookmark_name','$bookmark_url')";
            $conn->exec( $insertQuery );
            header( "location:../../index.php" );
        }
    } catch ( PDOException $e ) {
        echo "Error: ".$e->getMessage();
    }
}

?>